const argon2 = require('argon2');

const hash = '$argon2id$v=19$m=4096,t=3,p=1$b8btrZIlAqYR/auTNgbZuA$wwv60vEQHIHUYvVYpl1a8NmctXBJwYSnbkolOLBKoH4';
const passwd = 'passwortundso';

argon2.verify(hash, passwd, {type: argon2.argon2id}).then(match => {
    if (match) {
      // password match
      console.log('Passwords match');
    } else {
      // password did not match
      console.log('Passwords do not match');
    }
  }).catch(err => {
    // internal failure
    console.log('ERROR:' + err)
  });