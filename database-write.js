const Influx = require('influx');
const influx = new Influx.InfluxDB({
  host: 'localhost',
  database: 'sensordata',
  schema:[{
    measurement: 'temphum',
    fields: {
      temp: Influx.FieldType.FLOAT,
      hum: Influx.FieldType.FLOAT
    },
    tags: [
      'device','user'
    ]
  }
  ]
});

influx.writePoints([{

        measurement: 'temphum',
        tags: {device:'testgerät',user:'testuser' },
        fields: {temp:'15', hum:'9'}
        }
    ]).then(()=>{
        console.log('written data to database');
    }).catch(err =>{
        console.log(`Error on saving the data to the Database! ${err.stack}`);
    })