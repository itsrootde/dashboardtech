const Joi = require('joi');

//Schema for data validation
const schema = Joi.object().keys({
    username: Joi.string().alphanum().min(3).max(30).required(),
    password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
    devicename: Joi.string().regex(/^[a-zA-Z0-9_ ]{3,30}$/),
    devicetype: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/)
}).with('username','password');

module.exports = schema;