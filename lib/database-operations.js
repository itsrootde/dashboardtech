const mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
mongoose.connect('mongodb://localhost:27017/hometech', {useNewUrlParser : true});
// Database Schema
var userSchema = new Schema({
    name: String,
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    admin: Boolean,
    meta: {
      age: Number,
      website: String
    },
    created_at: Date,
    updated_at: Date,
    last_login: Date
});
  var User = mongoose.model('User', userSchema, 'login');

var deviceSchema = new Schema({
  userid: String,
  devicename: String,
  devicetype: String,
  devicelogintoken: String,
  created_at: Date,
  last_login: Date
});

var Device = mongoose.model('Device', deviceSchema, 'device');

module.exports = {
  User: User,
  Device: Device
}