const express = require('express');
const router = express.Router();
const cookieParser = require('cookie-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const uuidv4 = require('uuid/v4');
const crypto = require('crypto');
 

router.use(session({
    key: 'test', // currently for dev
    secret: crypto.createHmac('sha256', uuidv4()).update(uuidv4()).digest('hex'),
    maxAge: 60000,
    httpOnly: true,
    resave: false,
    saveUninitialized: true,
    cookie: {
        expires: 600000,
        secure: false, // has to be set true for production!!!
        signed: true
    },
    store: new MongoStore({ 
        url: 'mongodb://localhost/hometech'
      })
  }));

module.exports = router;