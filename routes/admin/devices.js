const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');
const DB = require('../../lib/database-operations');
const schema = require('../../lib/data-validation');
const Joi = require('joi');
const uuidv4 = require('uuid/v4');
const crypto = require('crypto');
const argon2 = require('argon2');

router.use(bodyparser.urlencoded({extended:true}));

router.all('/*', (req, res, next) =>{
    req.app.locals.layout = 'admin';
    next();
});

router.get ('/devices', (req, res) =>{
    res.render('admin/devices');
});

router.post('/api/devices/create', function (req, res){
    console.log("Recieved API call");
    console.log('Devicename: ' + req.body.devicename);
    console.log('Devicetype: ' + req.body.devicetype);
    var date = new Date();
    if(req.session.active){
            DB.User.findOne({_id: req.session.accessData},(err, doc) => {
                console.log(doc._id);
                console.log('generate token...');
                token = crypto.createHmac('sha256', uuidv4()).update(uuidv4()).digest('hex');
                argon2.hash(token, {
                    type: argon2.argon2id
                }).then(hash => {
                    Joi.validate({ devicename: req.body.devicename, devicetype: req.body.devicename}, schema, function(err, value){
                        if(!err){
                            var Device = new DB.Device({
                                userid: doc._id,
                                devicename: req.body.devicename,
                                devicetype: req.body.devicetype,
                                devicelogintoken: hash,
                                created_at: date.getTime()
                            });
                            Device.save((err, doc) => {
                                if(err) throw err;
                                console.log(doc._id);
                                res.send({
                                    devicename: req.body.devicename,
                                    token: token
                                });
                            });
                        }else{res.send('ERROR');}
                    });
                
            });
        });

    }else{
        res.send('User not authenticated');
    }

   
});

module.exports = router;