const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');
const argon2 = require('argon2');
const Joi = require('joi');
const schema = require('../../lib/data-validation');
const DB = require('../../lib/database-operations');

//enable body parser
router.use(bodyparser.urlencoded({extended:true}));

//get post request
router.post('/api/auth/login', function (req, res){
    console.log("Recieved API call for authentication");
    if(req.session.active){
        console.log('User already logged in - no need for reauthentication');
        res.send('already logged in!');
    }else{
    username = req.body.username;
    password = req.body.password;
    Joi.validate({ username: username, password: password}, schema, function(err, value){
        if(!err){
            DB.User.findOne({ username: username }, function(err, user) {
                //if (err) throw err;
                if(user){
                    argon2.verify(user.password, password).then(match => {
                        if (match) {
                          var date = new Date();
                          DB.User.findOneAndUpdate({username:username},{last_login : date.getTime()}, (err, doc) =>{
                            if(err){
                                console.log('ERROR in databaes execution ' + err);
                            }
                            console.log(doc._id);
                          });
                          res.send('Credentials OK');
                          console.log(req.sessionID); // print out sessionID for development
                          req.session.accessData = doc._id; // save username to session
                          req.session.active = true;
                          req.session.save(); // save the session to the session store
                        } else {
                        res.send('Credentials WRONG');
                        }
                      }).catch(err => {
                        res.send('Credentials WRONG');
                        console.log(err);
                      });
                }else{
                    res.send('Credentials WRONG');
                }
              });
            
        }else{res.send('ERROR');}
    });
}
});

module.exports = router;