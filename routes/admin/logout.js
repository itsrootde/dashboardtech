const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');

//enable body parser
router.use(bodyparser.urlencoded({extended:true}));

//get post request
router.post('/api/auth/logout', function (req, res){
    if(req.session.accessData){
        console.log("Recieved API call for deauthentication");
        req.session.destroy();
        res.clearCookie('test'); // currently for dev
        res.send('User logged out');
    }else{
        res.send('Not logged in - no need for deauthentication');
    }
    

});

module.exports = router;