const express = require('express');
const router = express.Router();
const bodyparser = require('body-parser');
const argon2 = require('argon2');
const Joi = require('joi');
const schema = require('../../lib/data-validation');
const DB = require('../../lib/database-operations');

//enable body parser
router.use(bodyparser.urlencoded({extended:true}));

//get post request
router.post('/api/auth/register', function (req, res){
    console.log("Recieved API call for registration");
    username = req.body.username;
    password = req.body.password;
    Joi.validate({ username: username, password: password}, schema, function(err, value){
        if(!err){
        argon2.hash(req.body.password, {
            type: argon2.argon2id
        }).then(hash => {
            console.log('Password hashed: ' + hash);
            var date = new Date();
            var newUser = new DB.User({
                username: username,
                password: hash,
                created_at: date.getTime()
            });
            newUser.save(function(err) {
                //if(err) throw err;
                if(!err){
                    console.log('user created');
                    res.send('User created');
                }else{
                    res.send('Nutzername existiert bereits');
                }
            });
            
        }).catch(err => {
            console.log('ERROR' + err);
            res.send('ERROR');
        });
        }else{res.send('ERROR');}
    });
});

module.exports = router;