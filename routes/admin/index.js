const express = require('express');
const router = express.Router();

router.all('/*', (req, res, next) =>{
    req.app.locals.layout = 'admin';
    next();
});

router.get ('/', (req, res) =>{
    res.render('admin/index');
});

router.get ('/login', (req, res) =>{
    res.render('admin/login');
});

router.get ('/register', (req, res) =>{
    res.render('admin/register');
});


module.exports = router;