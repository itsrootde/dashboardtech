const express = require('express');
const app = express();
const path = require('path');
const exphb = require('express-handlebars');

app.use(express.static(path.join(__dirname, 'public')));

const session = require('./routes/admin/session');
app.use('/*', session);

app.engine('handlebars',exphb({defaultLayout: 'home'}));
app.set('view engine', 'handlebars');

const home = require('./routes/home/index');
app.use('/', home);

const dashboard = require('./routes/admin/index');
app.use('/dash', dashboard);

const devices = require('./routes/admin/devices');
app.use('/devices', devices);

app.post('/api/devices/create', devices);

const register = require('./routes/admin/register');
app.post('/api/auth/register', register);

const login = require('./routes/admin/login');
app.post('/api/auth/login', login);

const logout = require('./routes/admin/logout');
app.post('/api/auth/logout', logout);



app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
  });

app.listen(4334, ()=>{

    console.log(`listing on port 4334`);

});