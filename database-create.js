const Influx = require('influx');
const influx = new Influx.InfluxDB({
  host: 'localhost',
  database: 'sensordata',
  schema:[{
    measurement: 'temphum',
    fields: {
      temp: Influx.FieldType.FLOAT,
      hum: Influx.FieldType.FLOAT
    },
    tags: [
      'device','user'
    ]
  }]

});

influx.createDatabase('sensordata');